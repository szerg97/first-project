package com.example.pollbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "poll_questions")
@Getter
@Setter
@NoArgsConstructor
public class PollQuestion {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;
    private String question;
    @Enumerated(EnumType.STRING)
    private QuestionType type;

    @JsonIgnore(value = true)
    @OneToMany(targetEntity = Answer.class, mappedBy = "question")
    private List<Answer> answers;

    public PollQuestion(String question, QuestionType type) {
        this.question = question;
        this.type = type;
    }

    public void copyFrom(PollQuestion pollQuestion) {
        this.question = pollQuestion.getQuestion();
        this.type = pollQuestion.getType();
    }
}
