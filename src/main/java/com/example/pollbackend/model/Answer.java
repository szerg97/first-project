package com.example.pollbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "answers")
@Getter
@Setter
@NoArgsConstructor
public class Answer {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;
    private String text;
    @ManyToOne(targetEntity = PollQuestion.class)
    private PollQuestion question;

    public Answer(String text, PollQuestion question) {
        this.text = text;
        this.question = question;
    }

    public void copyFrom(Answer answer) {
        this.text = answer.getText();
        this.question = answer.getQuestion();
    }
}
