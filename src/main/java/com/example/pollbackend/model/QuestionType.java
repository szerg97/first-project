package com.example.pollbackend.model;

public enum QuestionType {
    SELECT_ONE,
    SELECT_MORE,
    FREE_TEXT
}
