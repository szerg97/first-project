package com.example.pollbackend;

import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.PollQuestion;
import com.example.pollbackend.model.QuestionType;
import com.example.pollbackend.repository.AnswerRepository;
import com.example.pollbackend.repository.PollQuestionRepository;
import com.example.pollbackend.service.Seeder;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

@SpringBootApplication
@AllArgsConstructor
public class PollBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PollBackendApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {

			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry
						.addMapping("/**");
			}
		};
	}

	@Bean
	public CommandLineRunner commandLineRunner(
			Seeder seeder
	){
		return args -> {
			seeder.seed();
		};
	}
}
