package com.example.pollbackend.service;

import com.example.pollbackend.model.PollQuestion;
import com.example.pollbackend.repository.PollQuestionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PollQuestionService {

    private final PollQuestionRepository questionRepository;

    public List<PollQuestion> getAll(){
        return this.questionRepository.findAll();
    }

    public PollQuestion getOne(Long id){
        Optional<PollQuestion> optional = this.questionRepository.findById(id);
        if (optional.isPresent())
            return optional.get();
        throw new RuntimeException("Could not found");
    }

    public PollQuestion addOne(PollQuestion question) throws Exception{
        this.questionRepository.save(question);
        return question;
    }

    public PollQuestion deleteOneById(Long id){
        PollQuestion question = getOne(id);
        if (question != null){
            this.questionRepository.delete(question);
            return question;
        }
        else
            throw new RuntimeException("Could not delete");
    }

    public PollQuestion updateOne(PollQuestion pAnswer){
        PollQuestion question = getOne(pAnswer.getId());
        if (question != null) {
            question.copyFrom(pAnswer);
            this.questionRepository.save(question);
            return question;
        }
        throw new RuntimeException("Could not update");
    }
}
