package com.example.pollbackend.service;

import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.PollQuestion;
import com.example.pollbackend.model.QuestionType;
import com.example.pollbackend.repository.AnswerRepository;
import com.example.pollbackend.repository.PollQuestionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@AllArgsConstructor
public class Seeder {

    private final PollQuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    public void seed(){
        PollQuestion p1 = new PollQuestion(
                "Which javascript framework do you like the most?",
                QuestionType.SELECT_ONE
        );
        PollQuestion p2 = new PollQuestion(
                "Which IDEs do you like to work with?",
                QuestionType.SELECT_MORE
        );
        PollQuestion p3 = new PollQuestion(
                "What's your proudest moment so far?",
                QuestionType.FREE_TEXT
        );

        questionRepository.saveAll(Arrays.asList(p1, p2, p3));

        Answer a1 = new Answer(
                "Angular",
                p1

        );
        Answer a2 = new Answer(
                "React",
                p1
        );
        Answer a3 = new Answer(
                "Vue",
                p1
        );
        Answer a4 = new Answer(
                "Svelte",
                p1
        );
        Answer a5 = new Answer(
                "React",
                p1
        );

        Answer aa1 = new Answer(
                "Webstorm",
                p2

        );
        Answer aa2 = new Answer(
                "IntelliJ IDEA",
                p2
        );
        Answer aa3 = new Answer(
                "VSCode",
                p2
        );
        Answer aa4 = new Answer(
                "Sublime",
                p2
        );

        answerRepository.saveAll(Arrays.asList(a1, a2, a3, a4, a5, aa1, aa2, aa3, aa4));
    }
}
