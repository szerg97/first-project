package com.example.pollbackend.service;

import com.example.pollbackend.model.Answer;
import com.example.pollbackend.repository.AnswerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AnswerService {

    private final AnswerRepository answerRepository;

    public List<Answer> getAll(){
        return this.answerRepository.findAll();
    }

    public Answer getOne(Long id){
        Optional<Answer> optional = this.answerRepository.findById(id);
        if (optional.isPresent())
            return optional.get();
        throw new RuntimeException("Could not found");
    }

    public Answer addOne(Answer answer) throws Exception{
        this.answerRepository.save(answer);
        return answer;
    }

    public Answer deleteOneById(Long id){
        Answer answer = getOne(id);
        if (answer != null){
            this.answerRepository.delete(answer);
            return answer;
        }
        else
            throw new RuntimeException("Could not delete");
    }

    public Answer updateOne(Answer pAnswer){
        Answer answer = getOne(pAnswer.getId());
        if (answer != null) {
            answer.copyFrom(pAnswer);
            this.answerRepository.save(answer);
            return answer;
        }
        throw new RuntimeException("Could not update");
    }
}
