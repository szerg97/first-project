package com.example.pollbackend.repository;

import com.example.pollbackend.model.PollQuestion;
import com.example.pollbackend.model.QuestionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PollQuestionRepository extends JpaRepository<PollQuestion, Long> {
    List<PollQuestion> findQuestionsByType(QuestionType type);
}
