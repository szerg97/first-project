package com.example.pollbackend.repository;

import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.PollQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    List<Answer> findAnswersByQuestion(Long id);
}
