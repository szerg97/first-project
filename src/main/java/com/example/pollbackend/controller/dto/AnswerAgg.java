package com.example.pollbackend.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnswerAgg {

    private String text;
    private Integer count;

    public AnswerAgg(String text, Integer count) {
        this.text = text;
        this.count = count;
    }
}
