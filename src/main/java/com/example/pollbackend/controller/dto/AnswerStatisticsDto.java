package com.example.pollbackend.controller.dto;

import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.PollQuestion;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
public class AnswerStatisticsDto {

    private String question;
    private String type;
    private List<AnswerAgg> answers = new ArrayList<>();

    public AnswerStatisticsDto(PollQuestion question) {
        this.question = question.getQuestion();
        this.type = question.getType().toString();
        Map<String, Integer> map = new HashMap<>();

        List<Answer> distinct = question.getAnswers().stream().distinct().collect(Collectors.toList());
        distinct.forEach(x -> {
            List<Answer> collect = question.getAnswers().stream().filter(y -> y.getText().equals(x.getText())).collect(Collectors.toList());
            map.put(collect.get(0).getText(), collect.size());
        });

        map.forEach((k, v) -> this.answers.add(new AnswerAgg(k, v)));
        this.answers = this.answers.stream().sorted(Comparator.comparingInt(AnswerAgg::getCount).reversed()).collect(Collectors.toList());
    }
}
