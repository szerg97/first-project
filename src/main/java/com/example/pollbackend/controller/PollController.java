package com.example.pollbackend.controller;

import com.example.pollbackend.controller.dto.AnswerStatisticsDto;
import com.example.pollbackend.model.Answer;
import com.example.pollbackend.model.PollQuestion;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.pollbackend.service.AnswerService;
import com.example.pollbackend.service.PollQuestionService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")
@AllArgsConstructor
public class PollController {

    private final AnswerService answerService;
    private final PollQuestionService questionService;

    @GetMapping(path = "/questionnaire")
    public ResponseEntity<List<PollQuestion>> getAllQuestions(){
        return ResponseEntity.ok(this.questionService.getAll()
                .stream()
                .sorted(Comparator.comparing(PollQuestion::getQuestion)).collect(Collectors.toList()));
    }

    @GetMapping(path = "/answers")
    public ResponseEntity<List<Answer>> getAllAnswers(){
        return ResponseEntity.ok(this.answerService.getAll());
    }

    @GetMapping(path = "/answer-statistics/{questionId}")
    public ResponseEntity<AnswerStatisticsDto> getAnswerStatistics(
            @PathVariable(name = "questionId", required = true)Long questionId){
        PollQuestion question = this.questionService.getOne(questionId);
        AnswerStatisticsDto dto = new AnswerStatisticsDto(question);
        return ResponseEntity.ok(dto);
    }

}
